package test;

import services.StorePersonDataService;
import egs.bean.Person;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by haykh on 4/23/2019.
 */
public class StorePersonDataServiceTest {

  @Test
  public void storeTest() {
    StorePersonDataService service = new StorePersonDataService();
    Person person = new Person();
    boolean store = service.store(person);
    Assert.assertTrue(store);
  }

}
